import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoComponent } from './info/info.component';
import { ItemsComponent } from './items/items.component';
import { ResultComponent } from './result/result.component';
import { TestComponent } from './test/test.component';

const routes: Routes = [
  {path:"",redirectTo:'/test', pathMatch:"full"},
  {path:"tested/:id",component:ResultComponent,
   children:[
    {path:"info", component: InfoComponent},
    {path:"items", component:ItemsComponent}
   ]},
  {path:"test", component: TestComponent},
  {path:"result",component: ResultComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const RoutingModule = [
  TestComponent,
  ResultComponent,
  InfoComponent,
  ItemsComponent
]
