import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }
  public lists = [
    { "id": 1, data2: "done", data3: "fine" },
    { "id": 2, data2: "good", data3: "hi" },
    {"id": 3 ,data2: "go"}
    ];
}
