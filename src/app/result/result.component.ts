import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap, Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  constructor(private service : SharedService,private router:Router,private route:ActivatedRoute) { }
   id : any;
  public infoDetail: any;
 
  goback(){
    let select = this.id;
    this.router.navigate(['../']);
    // ,{relativeTo:this.route},{id: select}

  }
  ngOnInit():void {
    this.route.paramMap.subscribe((param: ParamMap)=>{
      this.id= param.get('id');
    })
    // // this.infoDetail = this.service.lists;
    // this.route.paramMap.subscribe((param:ParamMap)=>{
    //   let id = parseInt(param.get('id'));
    //   this.infoDetail =id;
    // }); //this.infoDetail = id;
    // let id =parseInt(this.route.snapshot.paramMap.get('id'));
    // this.infoDetail =id; 
  }

}
