import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'jet';
  
  get name(){
    return this.someData.get('name');
  }

  get password(){
    return this.someData.get('password');
  }

  constructor(private info:FormBuilder){

  }
  someData = this.info.group({
    name :['',Validators.required],
    password: ['',[Validators.required,Validators.minLength(4)]]
  });
  public things = "one";
  public data = new Date();
  time = this.data.getDay() +"/"+ this.data.getMonth() +"/"+ this.data.getFullYear();
  public said = false;
  public lists = [
    { data1: "ok", data2: "done", data3: "fine" },
    { data1: "bye", data2: "good", data3: "hi" }
    ];
  job(){

    this.said = !this.said; 
  }
  onSubmit(){
    console.log(this.someData.value);
  }
}
