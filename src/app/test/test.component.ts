import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { SharedService } from '../shared.service';


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(private router:Router,private route: ActivatedRoute,private service:SharedService,private info: FormBuilder) { }

  collection:any = [];

  
  
  
    onselet(list:any){
      this.router.navigate(["/tested",list.id]);
    }
  ngOnInit(): void {
    this.collection = this.service.lists;
  }

}
